/// <reference types="cypress" />

import loc from '../../support/locators'


describe('Should', ()=>{
    before(()=> {
        cy.visit('../app/login.html')
    })

    afterEach(()=>{
        cy.visit('../app/login.html')
    })

    it('login - sucess', () => {
        cy.get(loc.LOGIN.USER).type('user')
        cy.get(loc.LOGIN.PASSWORD).type('123')
        cy.get(loc.LOGIN.BTN_LOGIN).click()
        cy.get('.toast-title').should('contain', 'Bem Vindo')
        cy.get('.toast-message').should( 'contain', 'login realizado com sucesso')
        cy.get('body > :nth-child(1)').should('contain', "There's nothing to see here.")
    })

    it('login - blank user', () =>{
        cy.get('[type="password"]').type('123')
        cy.get('.button').click()
        cy.wait(1000)
        cy.get('.toast-title').should('contain', 'Erro')
        cy.get('.toast-message').should( 'contain', 'Há campos em branco')
    })

    it('login - blank password', () =>{
        cy.get('[type="text"]').type('user')
        cy.get('.button').click()
        cy.wait(1000)
        cy.get('.toast-title').should('contain', 'Erro')
        cy.get('.toast-message').should( 'contain', 'Há campos em branco')
    })

    it('login - blank inputs', () =>{
        cy.get('.button').click()
        cy.wait(1000)
        cy.get('.toast-title').should('contain', 'Erro')
        cy.get('.toast-message').should( 'contain', 'Há campos em branco')
    })

    it('login - wrong credentials', () => {
        cy.get('[type="text"]').type('user-test')
        cy.get('[type="password"]').type('123')
        cy.get('.button').click()
        cy.get('.toast-title').should('contain', 'Erro')
        cy.get('.toast-message').should( 'contain', 'Dados inválidos')
    })

})

//table//td[contains(., 'texto)]/../i[@class='class name']