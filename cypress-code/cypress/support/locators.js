const locators = {
    LOGIN: {
        USER: '[type="text"]',
        PASSWORD: '[type="password"]',
        BTN_LOGIN: '.button'
    }
}


export default locators;